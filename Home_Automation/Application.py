

class Application:
    def __init__(self):
        #self.led= "green" indicates smart plug is connected to WIFI
        #self.led= "red" indicates smart plug is in use
        self.led = "green"
        self.energyconsumption = 0
        self.basicconsumption = 5 #Assuming the consumption is in KW
        self.timer = 0
        self.timer_hardcode = 40  #Assuming the timer runs for 40 minutes

    def smart_plug_switch(self, status):
        if status == 1:
            self.timer = 1
            self.led = "red"
            self.energyconsumption = self.basicconsumption
            print('switch is on')
            return 'Switch is on'

        elif status == 0:
            self.timer = 0
            self.led = "green"
            self.energyconsumption = self.basicconsumption * self.timer_hardcode
            print('switch is off')
            return 'Switch is off'

    def energy_consumption(self):
        print('The total energy consumption by the switch is: '+str(self.energyconsumption))
        return self.energyconsumption

    def appliance_in_use(self):
        if self.led == "red" :
            print('Appliance in use')
            return 'Appliance in use'
        else:
            print('Appliance not in use')
            return 'Appliance not in use'











