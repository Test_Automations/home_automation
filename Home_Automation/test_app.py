import unittest
import Login
import Application


class TestEndToEndTest(unittest.TestCase):

    # Module/component tests
    def test_login(self):
        result = Login.login_credentials(username='Admin', password='admin')
        self.assertEqual(result, True)

    def test_switch_on(self):
        app = Application.Application()
        result = app.smart_plug_switch(1)
        self.assertEqual(result, 'Switch is on')

    def test_switch_off(self):
        app = Application.Application()
        result = app.smart_plug_switch(0)
        self.assertEqual(result, 'Switch is off')

    def test_basic_energy_consumption(self):
        app = Application.Application()
        app.smart_plug_switch(1)
        result = app.energy_consumption()
        assert result == 5

    def test_complete_energy_consumption(self):
        app = Application.Application()
        app.smart_plug_switch(1)
        app.smart_plug_switch(0)
        result = app.energy_consumption()
        assert result == 200

    def test_appliance_in_use(self):
        app = Application.Application()
        app.smart_plug_switch(1)
        result = app.appliance_in_use()
        assert result == 'Appliance in use'

    def test_appliance_not_use(self):
        app = Application.Application()
        result = app.appliance_in_use()
        assert result == 'Appliance not in use'

    #End To End Test Cases 

    def test_login_to_switch_on(self):
        app = Application.Application()
        Login.login_credentials(username='Admin', password='admin')
        result = app.smart_plug_switch(1)
        self.assertEqual(result, 'Switch is on')

    def test_login_to_energyconsumption(self):
        app = Application.Application()
        Login.login_credentials(username='Admin', password='admin')
        app.smart_plug_switch(1)
        app.smart_plug_switch(0)
        result = app.energy_consumption()
        assert result == 200

    def test_login_to_appliance_in_use(self):
        app = Application.Application()
        Login.login_credentials(username='Admin', password='admin')
        app.smart_plug_switch(1)
        result = app.appliance_in_use()
        assert result == 'Appliance in use'


if __name__ == '__main__':
    unittest.main()
