# This is a sample Python script.
import Application
import argparse
import Login


def execute(select, username=' ', password=' '):
    print('Onboarding process started....')
    print('Installing the application')
    login_status = Login.login_credentials(username, password)
    # login_status = True
    if login_status == True:
        app = Application.Application()
        # 0:Switch off; 1:Switch on; 2:Energy Consumption; 3:Appliance Status
        if select == 0:
            app.smart_plug_switch(0)
        elif select == 1:
            app.smart_plug_switch(1)
        elif select == 2:
            app.energy_consumption()
        elif select == 3:
            app.appliance_in_use()
    else:
        print('Login Failed')
        print('If new user, please follow the onboarding process')


def main(select):
    execute(select)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('select', metavar='N', type=int,
                        help='select between the integers 0:Switch off; 1:Switch on; 2:Energy Consumption; 3:Appliance Status:')

    parser.add_argument('--call', dest='accumulate', action='store_const',
                        const=sum, default=max,
                        help='select the funcion you want to call')

    args = parser.parse_args()

    main(args.select)
